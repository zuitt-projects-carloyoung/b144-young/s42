// Retrieve an element from the webpage
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name');
// const txtFullName = document.querySelector('#txt-first-name'&&'#txt-last-name')
// console.log(document)
//"document refers to the whole webpage and "querySelector is used to select a specific object (HTML ekement) from the document.

// ALternatively, we can use getElement functions to retrieve the elements
//Syntax:
//document.getElementById('txt-first-name') 
//document.getElementByClassName('txt-inputs') 
//document.getElementByTagName('input')

// Performs an action when an event triggers

(txtFirstName,txtLastName).addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;

	// console.log(event.target)
	// console.log(event.target.value)
})

txtFirstName.addEventListener('keyup', (event) => {
	// spanFullName.innerHTML = txtFirstName.value;

	// event.target contains the element where the event happened
	console.log(event.target)
	// event.target.value gets the value of the input object
	console.log(event.target.value)
})


// txtLastName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtLastName.value;

// 	// console.log(event.target)
// 	// console.log(event.target.value)
// })

// txtLastName.addEventListener('keyup', (event) => {
// 	// spanFullName.innerHTML = txtFirstName.value;

// 	// event.target contains the element where the event happened
// 	console.log(event.target)
// 	// event.target.value gets the value of the input object
// 	console.log(event.target.value)
// })
